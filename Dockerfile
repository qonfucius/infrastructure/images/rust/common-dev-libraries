ARG RUST_VERSION=rust:alpine
FROM $RUST_VERSION

RUN apk add --no-cache clang musl-dev openssl-dev
